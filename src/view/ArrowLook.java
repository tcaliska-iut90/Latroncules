package view;

import boardifier.model.GameElement;
import boardifier.view.ConsoleColor;
import boardifier.view.ElementLook;
import model.Arrow;

public class ArrowLook extends ElementLook {

    public ArrowLook(GameElement element){
        super(element, 1, 1);
    }

    @Override
    protected void render() {

    }
}
